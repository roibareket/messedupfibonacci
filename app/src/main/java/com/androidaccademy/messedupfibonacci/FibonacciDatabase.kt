package com.androidaccademy.messedupfibonacci

import android.os.Looper
import java.lang.Exception
import java.lang.Thread.sleep

class FibonacciDatabase {
	fun saveToDb(any: Any) {
		if (Looper.myLooper() == Looper.getMainLooper()) {
			throw Exception("You should not save to db on main thread!")
		}
		sleep(1000)
	}
}
