package com.androidaccademy.messedupfibonacci

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.snakydesign.livedataextensions.startWith
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.Observables
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject


class FibonacciViewModel : ViewModel() {

	private val fibonacciDatabase = FibonacciDatabase()
	private val fibNum1 = PublishSubject.create<Int>()
	private val fibNum2 = PublishSubject.create<Int>()
	private val fibonacciResult = Observable.never<String>()
	private val fibExerciseLiveData = MutableLiveData("")

	val fibNum1LiveData: LiveData<String> = MutableLiveData("0")

	val fibNum2LiveData: LiveData<String> = MutableLiveData("1")

	val fibResultLiveData: LiveData<String> = MutableLiveData("1")

	fun fibExerciseLiveData(): LiveData<String> = fibExerciseLiveData

	fun calculateNextFibonacci(number1: String, number2: String) = Unit

	fun resetFibonacci(number1: String, number2: String) = Unit
}
