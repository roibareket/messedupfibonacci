package com.androidaccademy.messedupfibonacci

import android.view.View
import com.androidaccademy.messedupfibonacci.databinding.ActivityMainBinding

class FibonacciHandler(private val binding: ActivityMainBinding, val viewModel: FibonacciViewModel) {

	fun calculateNextFibonacci(view: View){
		viewModel.calculateNextFibonacci(binding.fibNum1.text.toString(), binding.fibNum2.text.toString())
	}

	fun resetFibonacci(view: View){
		viewModel.resetFibonacci(binding.fibNum1.text.toString(), binding.fibNum2.text.toString())
	}
}
