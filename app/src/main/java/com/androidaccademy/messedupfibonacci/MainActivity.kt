package com.androidaccademy.messedupfibonacci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.androidaccademy.messedupfibonacci.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

	val viewModel by lazy { ViewModelProviders.of(this).get(FibonacciViewModel::class.java) }
	lateinit var binding: ActivityMainBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
		binding.lifecycleOwner = this
		binding.handler = FibonacciHandler(binding, viewModel)
	}
}
